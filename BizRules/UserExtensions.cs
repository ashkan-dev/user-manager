﻿using Model;
using Model.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace BizRules
{
    internal static class UserExtensions
    {
        internal static void GetCreateAddr(this User user, CreateUserRequest data)
        {
            StringBuilder builder = new StringBuilder(data.Address);
            if (!string.IsNullOrEmpty(data.City))
            {
                if (builder.Length == 0)
                {
                    builder.Append(data.City);
                }
                else
                {
                    builder.Append($", {data.City}");
                }
            }

            if (!string.IsNullOrEmpty(data.State))
            {
                if (builder.Length == 0)
                {
                    builder.Append(data.State);
                }
                else
                {
                    builder.Append($", {data.State}");
                }
            }

            if (!string.IsNullOrEmpty(data.PinCode))
            {
                if (builder.Length == 0)
                {
                    builder.Append(data.PinCode);
                }
                else
                {
                    builder.Append($", {data.PinCode}");
                }
            }

            if (!string.IsNullOrEmpty(data.Country))
            {
                if (builder.Length == 0)
                {
                    builder.Append(data.Country);
                }
                else
                {
                    builder.Append($", {data.Country}");
                }
            }

            user.FullAddress = builder.ToString();
        }
        
        internal static void GetUpdAddr(this User user, UpdUserRequest data)
        {
            StringBuilder builder = new StringBuilder(data.Address);
            if (!string.IsNullOrEmpty(data.City))
            {
                if (builder.Length == 0)
                {
                    builder.Append(data.City);
                }
                else
                {
                    builder.Append($", {data.City}");
                }
            }

            if (!string.IsNullOrEmpty(data.State))
            {
                if (builder.Length == 0)
                {
                    builder.Append(data.State);
                }
                else
                {
                    builder.Append($", {data.State}");
                }
            }

            if (!string.IsNullOrEmpty(data.PinCode))
            {
                if (builder.Length == 0)
                {
                    builder.Append(data.PinCode);
                }
                else
                {
                    builder.Append($", {data.PinCode}");
                }
            }

            if (!string.IsNullOrEmpty(data.Country))
            {
                if (builder.Length == 0)
                {
                    builder.Append(data.Country);
                }
                else
                {
                    builder.Append($", {data.Country}");
                }
            }

            user.FullAddress = builder.ToString();
        } 
    }
}
