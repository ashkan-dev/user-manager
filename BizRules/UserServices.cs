﻿using Microsoft.AspNetCore.Mvc;
using Model;
using Model.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using System.Drawing;
using BizRuleInterfaces;

namespace BizRules
{
    public class UserServices : IUser
    {
        private readonly UserDbContext _context;

        public UserServices(
            UserDbContext context
        )
        {
            _context = context;
        }

        public int Create(CreateUserRequest data)
        {
            var user = new User
            {
                Name = data.Name,
                Designation = data.Designation,
                JoiningDate = data.JoiningDate,
                Country = data.Country,
                ImagePath = "/Images/photo.png"
            };

            user.GetCreateAddr(data);
            _context.Users.Add(user);
            var insert = _context.SaveChanges();
            return insert;
        }

        public int Delete(Guid id)
        {
            var user = _context.Users.FirstOrDefault(u => u.UserId == id);
            if (user == null)
            {
                return 0;
            }
            
            _context.Users.Remove(user);
            var delete = _context.SaveChanges();
            return delete;
        }

        public string GetImagePath(Guid id)
        {
            var user = _context.Users.FirstOrDefault(u => u.UserId == id);
            if (user == null)
            {
                return null;
            }

            return user.ImagePath;
        }

        public List<User> Paging(UsersPagingRequest data)
        {
            var items = _context.Users.Skip((data.PageNo - 1) * data.PageSize).Take(data.PageSize).ToList();
            return items;
        }

        public List<User> ReadAll()
        {
            var data = _context.Users.ToList();
            return data;
        }

        public User ReadById(Guid id)
        {
            var data = _context.Users.FirstOrDefault(u => u.UserId == id);
            return data;
        }

        public List<User> Search(string name)
        {
            var data = _context.Users.Where(s => s.Name == name).ToList();
            return data;
        }

        public List<User> Sort(string order)
        {
            var users = order == "asc"
                ? _context.Users.OrderBy(s => s.Name).ToList()
                : _context.Users.OrderByDescending(s => s.Name).ToList();

            return users;
        }

        public int Update(UpdUserRequest data)
        {
            var user = _context.Users.FirstOrDefault(u => u.UserId == data.Id);
            if (user == null)
            {
                return 0;
            }
            
            var item = new User
            {
                UserId = data.Id,
                Name = data.Name,
                Designation = data.Designation,
                JoiningDate = data.JoiningDate,
                Country = data.Country,
                ImagePath = data.ImagePath
            };

            item.GetUpdAddr(data);
            _context.Entry<User>(user).CurrentValues.SetValues(item);
            var upd = _context.SaveChanges();
            return upd;
        }
    }
}
