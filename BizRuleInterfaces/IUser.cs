﻿using Microsoft.AspNetCore.Mvc;
using Model;
using Model.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace BizRuleInterfaces
{
    public interface IUser
    {
        List<User> ReadAll();

        User ReadById(Guid id);

        List<User> Search(string name);

        List<User> Sort(string order);

        List<User> Paging(UsersPagingRequest data);

        int Create(CreateUserRequest data);

        int Update(UpdUserRequest data);

        int Delete(Guid id);

        string GetImagePath(Guid id);
    }
}
