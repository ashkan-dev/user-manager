﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using Model;
using Model.DTOs;
using BizRuleInterfaces;

namespace UserWebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUser _service;
        private readonly IHostingEnvironment _hostEnv;

        public UserController(
            IUser service,
            IHostingEnvironment hostingEnvironment
        )
        {
            _service = service;
            _hostEnv = hostingEnvironment;
        }

        [HttpGet]
        public IActionResult ReadAll()
        {
            var data = _service.ReadAll();
            return Ok(data);
        }

        [HttpGet]
        public IActionResult ReadById(ReadByUserIdRequest item)
        {
            var data = _service.ReadById(item.UserId);
            return Ok(data);
        }

        [HttpGet]
        public IActionResult Search(SearchUserRequest item)
        {
            var data = _service.Search(item.Name);
            return Ok(data);
        }

        [HttpGet]
        public IActionResult Sort(SortUsersRequest item)
        {
            var data = _service.Sort(item.SortOrder);
            return Ok(data);
        }

        [HttpGet]
        public IActionResult Paging(UsersPagingRequest data)
        {
            var items = _service.Paging(data);
            return Ok(items);
        }

        [HttpPost]
        public IActionResult Create(CreateUserRequest data)
        {
            var insert = _service.Create(data);
            return Ok(insert);
        }

        [HttpPut]
        public IActionResult Update(UpdUserRequest data)
        {
            var upd = _service.Update(data);
            if (upd == 0)
            {
                return StatusCode(515, ErrMsgs.USER_NOT_FOUND);
            }

            return Ok(upd);
        }

        [HttpDelete]
        public IActionResult Delete(DeleteUserRequest item)
        {
            var delete = _service.Delete(item.UserId);
            if (delete == 0)
            {
                return StatusCode(515, ErrMsgs.USER_NOT_FOUND);
            }

            return Ok(delete);
        }

        [HttpGet]
        public IActionResult GetImage(GetUserImgRequest item)
        {
            var data = _service.GetImagePath(item.Id);
            var path = _hostEnv.ContentRootFileProvider.GetFileInfo(data).PhysicalPath;
            var imageFileStream = System.IO.File.OpenRead(path);
            return File(imageFileStream, "image/png");
        }
    }
}
