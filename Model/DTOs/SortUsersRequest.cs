﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.DTOs
{
    public class SortUsersRequest
    {
        public string SortOrder { get; set; }
    }
}
