﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.DTOs
{
    public class UpdUserRequest
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Designation { get; set; }
        public DateTime JoiningDate { get; set; }
        public string ImagePath { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PinCode { get; set; }
        public string Country { get; set; }
    }
}
