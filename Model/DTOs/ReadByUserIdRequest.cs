﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.DTOs
{
    public class ReadByUserIdRequest
    {
        public Guid UserId { get; set; }
    }
}
