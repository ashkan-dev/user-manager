﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.DTOs
{
    public class GetUserImgRequest
    {
        public Guid Id { get; set; }
    }
}
