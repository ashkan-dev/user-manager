﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.DTOs
{
    public class UsersPagingRequest
    {
        public int PageNo { get; set; }
        public int PageSize { get; set; }
    }
}
