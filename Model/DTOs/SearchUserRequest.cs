﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.DTOs
{
    public class SearchUserRequest
    {
        public string Name { get; set; }
    }
}
