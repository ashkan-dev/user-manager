﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    [Table("User")]
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid UserId { get; set; }

        [CustomEmptyCheck]
        public string Name { get; set; }

        [CustomEmptyCheck]
        public string Designation { get; set; }

        public DateTime JoiningDate { get; set; }

        public string FullAddress { get; set; }

        [CustomEmptyCheck]
        public string Country { get; set; }

        public string ImagePath { get; set; }
    }
}
